#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ESP8266HTTPUpdateServer.h>
#include <EEPROM.h>
#include <PubSubClient.h>
#include <TinyGPS.h>
#include <stdio.h>


#define GSM_PWRKEY_PIN 10

const char* host = "esp8266-webupdate";
const char* ssid = "Irancell-TD-i40-A1_9274";
const char* password = "379B4AA1AA";
const char* mqtt_server = "88.99.155.208";

ESP8266WebServer httpServer(80);
ESP8266HTTPUpdateServer httpUpdater;
WiFiClient espClient;
PubSubClient client(espClient);
TinyGPS gps; 

void(* resetFunc) (void) = 0;

void setup_wifi(){
  delay(10);

  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void sub_callback(char* topic, byte* payload, unsigned int length){


  if (topic[0] == 'h' && topic[1] == 'a' && topic[2] == 'm' && topic[3] == 'i' && topic[4] == 'd')
  {
    if ((char)payload[0] == 'x') //calibration state
    {
      client.publish("log", "\" x RECEIVED.\"");
    }
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str(),"shelfix","shelfix1")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("log", "reconnected");
    } else {
      client.publish("log", "try again in 5 seconds to reconnect");
      delay(5000);
    }
  }
}

void printHex(byte *buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], HEX);
  }
}

void GSM_command(String cmd){
  Serial.println(cmd);
  delay(100);
}

bool read_response(String report, int timeout, String& response){
  String str;
  int loop_counter = 0;
  while(!Serial.available() && (loop_counter/10.0)<timeout){
    delay(100);
    httpServer.handleClient();
    loop_counter++;
  }
  response = Serial.readString();
  if (loop_counter != 0.0) {
    str = "loop_counter:" + String(loop_counter/10.0);
    client.publish("log", &str[0u]);
  }

  if (loop_counter/10.0 == timeout) {
    client.publish("log", "timeout");
  //  return false;
  }
  str = report + "->" + response;
  client.publish("log", &str[0u]);


  return true;
}

bool validate_response(String response, String val){
  String str;

  if(response.indexOf(val) != -1){
    //client.publish("log", "succeed");
    return true;
  }else{
    client.publish("bemine/log", "validate failed");
    return false;
  }
}

void GSM_powerOn(){
  delay(500);
  digitalWrite(GSM_PWRKEY_PIN,HIGH);
  delay(2700);
  digitalWrite(GSM_PWRKEY_PIN,LOW);
  delay(5000);
}

void GSM_powerOff(){
  delay(500);
  digitalWrite(GSM_PWRKEY_PIN,HIGH);
  delay(850);
  digitalWrite(GSM_PWRKEY_PIN,LOW);
  delay(500);
}

void GSM_setup(){
  String str;
  // pinMode(GSM_PWRKEY_PIN, OUTPUT);
  // digitalWrite(GSM_PWRKEY_PIN,LOW);
  client.publish("log","setup GSM");
  delay(1000);

  // GSM_powerOff();
  // client.publish("log","GSM off");
  // delay(5000);
  //
  // GSM_powerOn();
  // client.publish("log","GSM on");
  // delay(5000);
  //
  // GSM_powerOff();
  // client.publish("log","GSM off");
  // delay(5000);
  //
  // GSM_powerOn();
  // client.publish("log","GSM on");


  GSM_command("AT+QPOWD=1");
  read_response("AT+QPOWD=1", 10, str);
  validate_response(str, "NORMAL POWER DOWN");
  client.publish("log","GSM off");
  delay(1000);
  GSM_powerOn();
  client.publish("log","GSM on");
  read_response("boot ", 10, str);
}

void GSM_init() {
  String str;

  GSM_command("AT");
  read_response("AT", 10, str);
  validate_response(str, "OK");

  GSM_command("ATE0&W");    // turn off Echo mode
  read_response("ATE0&W", 10, str);
  validate_response(str, "OK");

  //GSM_command("AT+QADC?");    // Read ADC
  //read_response("AT+QADC?", 10, str);
  //validate_response(str, "OK");

  //GSM_command("AT+QREFUSECS=1,1");    // Refuse to Receive SMS/Incoming Call or Not
  //read_response("AT+QREFUSECS=1,1", 10, str);
  //validate_response(str, "OK");

  /**
   * make sure that the SIM card has registered to the network before anything
   */

  GSM_command("AT+QSIMSTAT?");    // SIM Inserted Status Reporting
  read_response("AT+QSIMSTAT?", 10, str);
  validate_response(str, "OK");

  //GSM_command("AT+QSIMDET=0,0,0");    // Enable/Disable SIM Card Detection
  //read_response("AT+QSIMDET=0,0,0", 10, str);
  //validate_response(str, "OK");

  GSM_command("AT+QSPN?");    // Get Service Provider Name from SIM
  read_response("AT+QSPN?", 10, str);
  validate_response(str, "OK");

  //GSM_command("AT+COPN");    // Read Operator Names
  //read_response("AT+COPN", 10, str);
  //validate_response(str, "OK");

  //GSM_command("AT+CFUN?");    // Phone Functionality: 1 = Full functionality
  //read_response("AT+CFUN?", 10, str);
  //validate_response(str, "OK");

  GSM_command("AT+CPIN?");    //
  read_response("AT+CPIN?", 10, str);
  validate_response(str, "OK");

  /**
   * Network querying and setting
   */
  GSM_command("AT+CSQ");    // Signal Quality Report
  read_response("AT+CSQ", 10, str);
  validate_response(str, "OK");

  GSM_command("AT+CREG?");    // Query register state of GSM network
  read_response("AT+CREG?", 10, str);    //<stat>=1 means GSM network is registered
  validate_response(str, "OK");

  GSM_command("AT+CGREG?");    // Query register state of GPRS network
  read_response("AT+CGREG?", 10, str);    // <stat>=1 means GPRS network is registered
  validate_response(str, "OK");

  GSM_command("AT+COPS?");    // Query the currently selected operator
  read_response("AT+COPS?", 10, str);
  validate_response(str, "OK");


  /**
   * Activate GPRS context
   */
  GSM_command("AT+CGATT=1");    // Attach to GPRS Service
  read_response("AT+CGATT=1", 10, str);
  validate_response(str, "OK");

  GSM_command("AT+CGDCONT=1,\"IP\",\"mtnirancell\"");    // Define PDP Context; PDP_type=IP, APN=mtnirancell
  read_response("AT+CGDCONT=1,\"IP\",\"mtnirancell\"", 10, str);
  validate_response(str, "OK");

  GSM_command("AT+CGACT=1,1");    // Activate GPRS context
  read_response("AT+CGACT=1,1", 10, str);
  validate_response(str, "OK");

  /**
   * Establish TCP/UDP connection
   */
   GSM_command("AT+QIMUX?");    // Control Whether or Not to Enable Multiple TCPIP
   read_response("AT+QIMUX?", 10, str);
   validate_response(str, "OK");

   GSM_command("AT+QIMODE=0");    // Select TCPIP Transfer Mode ; Non-Transparent=0, Transparent=1
   read_response("AT+QIMODE=0", 10, str);
   validate_response(str, "OK");

    GSM_command("AT+QIDNSIP=1");    // Use "domain name" as the address to establish TCP session, AT+QIDNSIP=0 : IP
    read_response("AT+QIDNSIP=1", 10, str);
    validate_response(str, "OK");

    // GSM_command("AT+QLTS");    //
    // read_response("AT+QLTS", 10, str);
    // validate_response(str, "OK");

    // GSM_command("AT+CTZU?");    //
    // read_response("AT+CTZU?", 10, str);
    // validate_response(str, "OK");

    GSM_command("AT+CTZU=0");    //
    read_response("AT+CTZU=0", 10, str);
    validate_response(str, "OK");

    GSM_command("AT+QNTP?");    //
    read_response("AT+QNTP?", 10, str);
    validate_response(str, "OK");

    // GSM_command("AT+QNTP");    //
    // read_response("AT+QNTP", 10, str);
    // validate_response(str, "OK");

    GSM_command("AT+QNTP=\"2.ir.pool.ntp.org\"");    //
    // delay(10000);
    // if (!client.connected()) {
    //   reconnect();
    // }
    // delay(10000);
    // if (!client.connected()) {
    //   reconnect();
    // }
    // delay(10000);
    // if (!client.connected()) {
    //   reconnect();
    // }
    delay(1000);
    read_response("AT+QNTP=\"2.ir.pool.ntp.org\"", 10, str);
    validate_response(str, "QNTP: 0"); //  Default SERVER: 210.72.145.44 2.ir.pool.ntp.org

    GSM_command("AT+CCLK?");    //
    read_response("AT+CCLK?", 10, str);
    validate_response(str, "OK");

    GSM_command("AT+QICSGP=1,\"mtnirancell\",\"\",\"\"");    // Select GPRS as the Bearer, apn=mtnirancell, user=<NULL>, pass=<NULL>
    read_response("AT+QICSGP=1,\"mtnirancell\",\"\",\"\"", 10, str);
    validate_response(str, "OK");

    GSM_command("AT+QIREGAPP=\"mtnirancell\",\"\",\"\"");    // Start TCPIP Task; apn=mtnirancell, user=<NULL>, pass=<NULL>
    read_response("AT+QIREGAPP=\"mtnirancell\",\"\",\"\"", 10, str);    // All of following commands should be executed together in sequence
    validate_response(str, "OK");

    GSM_command("AT+QIACT");    // Activate GPRS Context
    read_response("AT+QIACT", 10, str);   // Reboot the module if there is no response for AT+QIACT in 180s
    validate_response(str, "OK");

    GSM_command("AT+QILOCIP");    // Get Local IP Address
    read_response("AT+QILOCIP", 10, str);
    // validate_response(str, "OK");

    GSM_command("AT+QISDE?");    // Do not echo the data for AT+QISEND
    read_response("AT+QISDE?", 10, str);
    validate_response(str, "OK");
}

void GNSS_setup(){
  String str;
  client.publish("log","setup GNSS");

  GSM_command("AT+QGNSSC?");    // Control Power Supply of GNSS Module
  read_response("AT+QGNSSC?", 10, str);
  validate_response(str, "OK");

  GSM_command("AT+QGNSSC=1");    // Control Power Supply of GNSS Module
  read_response("AT+QGNSSC=1", 10, str);
  validate_response(str, "OK");
  client.publish("log","GNSS on");

  delay(5000);
}

void GNSS_init(){
  String str;
  char str_buf[20];
  char buf_w[64];
  char checkSum;

  /**
   * Time Synchronization
   */
  GSM_command("AT+QGNSSTS?");    // Get Time Synchronization Status for GNSS Module
  read_response("AT+QGNSSTS?", 10, str);
  validate_response(str, "OK");

  //  Online MTK NMEA checksum calculator: http://www.hhhh.org/wiml/proj/nmeaxor.html
  // GSM_command("AT+QGNSSCMD=0,\"$PMTK103*30\"");   //  PMTK_CMD_COLD_START
  // read_response("AT+QGNSSCMD=0,\"$PMTK103*30\"", 10, str);
  // validate_response(str, "PMTK");
  // delay(5000);
  //
  // GSM_command("AT+QGNSSCMD=0,\"$PMTK353,1,1,0,0,0*2B\"");       //  PMTK_API_SET_GNSS_SEARCH_MODE
  // read_response("AT+QGNSSCMD=0,\"$PMTK353,1,1,0,0,0*2B\"", 10, str);
  // validate_response(str, "PMTK");

  GSM_command("AT+QGNSSTS?");    // Get Time Synchronization Status for GNSS Module
  read_response("AT+QGNSSTS?", 10, str);
  validate_response(str, "OK");

  // GSM_command("AT+QNTP");    //
  // delay(5000);
  // read_response("AT+QNTP", 10, str);
  // validate_response(str, "OK");

  GSM_command("AT+CREG?;+CGREG?");    // Get Time Synchronization Status for GNSS Module
  read_response("AT+CREG?;+CGREG?", 10, str);
  validate_response(str, "OK");
  delay(2000);

  GSM_command("AT+CREG?;+CGREG?");    // Get Time Synchronization Status for GNSS Module
  read_response("AT+CREG?;+CGREG?", 10, str);
  validate_response(str, "OK");
  delay(2000);

  GSM_command("AT+CCLK?");    // Get Time Synchronization Status for GNSS Module
  read_response("AT+CCLK?", 10, str);
  validate_response(str, "OK");

  GSM_command("AT+QGNSSTS?");    // Get Time Synchronization Status for GNSS Module
  read_response("AT+QGNSSTS?", 10, str);
  validate_response(str, "OK");

  GSM_command("AT+QGREFLOC=35.78838,51.425595");    //
  read_response("AT+QGREFLOC=35.78838,51.425595", 10, str);
  validate_response(str, "OK");

  // GSM_command("AT+QGNSSEPO=1");    //
  // read_response("AT+QGNSSEPO=1", 10, str);
  // validate_response(str, "OK");
  //
  // GSM_command("AT+QGEPOAID");    //
  // read_response("AT+QGEPOAID", 10, str);
  // validate_response(str, "OK");

  GSM_command("AT+QGNSSRD=\"NMEA/GGA\"");    // Global Positioning System Fix Data
  read_response("AT+QGNSSRD=\"NMEA/GGA\"", 10, str);
  validate_response(str, "OK");

  GSM_command("AT+QGNSSRD=\"NMEA/VTG\"");    // Course Over Ground and Ground Speed
  read_response("AT+QGNSSRD=\"NMEA/VTG\"", 10, str);
  validate_response(str, "OK");

  GSM_command("AT+QCELLLOC=1");    // Get Current Location based on the density of network cells
  read_response("AT+QCELLLOC=1", 10, str);
  validate_response(str, "OK");
}

String GNSS_read(){
  String str, ret_str;

  // GSM_command("AT+QNTP?");    // Get Time Synchronization Status for GNSS Module
  // read_response("AT+QNTP?", 10, str);
  // validate_response(str, "OK");

  // GSM_command("AT+QNTP");    // Get Time Synchronization Status for GNSS Module
  // delay(5000);
  // read_response("AT+QNTP", 10, str);
  // validate_response(str, "OK");

  // GSM_command("AT+QLTS");    //
  // read_response("AT+QLTS", 10, str);
  // validate_response(str, "OK");

  GSM_command("AT+QGNSSTS?");    // Get Time Synchronization Status for GNSS Module
  read_response("AT+QGNSSTS?", 10, str);
  validate_response(str, "OK");

  GSM_command("AT+CCLK?");    // Get Time Synchronization Status for GNSS Module
  read_response("AT+CCLK?", 10, str);
  validate_response(str, "OK");

  GSM_command("AT+QGNSSRD=\"NMEA/GGA\"");    // Global Positioning System Fix Data
  read_response("AT+QGNSSRD=\"NMEA/GGA\"", 10, str);
  validate_response(str, "OK");
  ret_str = str;

  GSM_command("AT+QGNSSRD=\"NMEA/VTG\"");    // Course Over Ground and Ground Speed
  read_response("AT+QGNSSRD=\"NMEA/VTG\"", 10, str);
  validate_response(str, "OK");
  ret_str += str;

  // GSM_command("AT+QGNSSRD?");    //
  // loop_counter = 0;
  // while(!Serial.available() && (loop_counter/10.0)<10){
  //   delay(100);
  //   loop_counter++;
  // }
  // response = Serial.readString();
  //
  // if (gps.encode(response)) // Did a new valid sentence come in?
  //      newData = true;
  //
  // float flat, flon;
  // unsigned long age;
  // gps.f_get_position(&flat, &flon, &age);

  GSM_command("AT+QCELLLOC=1");    // Get Current Location based on the density of network cells
  read_response("AT+QCELLLOC=1", 10, str);
  validate_response(str, "OK");

  return ret_str;
}

void TCP_send(String domain, String query){
  char buf[4];
  String str;

  GSM_command("AT+CSQ");    // Signal Quality Report
  read_response("AT+CSQ", 10, str);
  validate_response(str, "OK");

  GSM_command("AT+QIOPEN=\"TCP\",\""+domain+"\",\"80\"");
  read_response("AT+QIOPEN=\"TCP\",\""+domain+"\",\"80\"", 10, str);
  validate_response(str, "OK");
  read_response("continue", 20, str);
  validate_response(str, "CONNECT");

  GSM_command("AT+QISEND");
  read_response("AT+QISEND", 20, str);
  validate_response(str, ">");

  Serial.print(query);
  // sprintf(buf,"%c%c%c%c%c%c",13, 10, 26,13, 10, 26); // CTRL(z)==26
  Serial.write(26);
  read_response("QUERY_SEND", 20, str);
  validate_response(str, "SEND OK");
  // read_response("SERVER_RESPONSE", 20, str);
  // validate_response(str, "salam");

  GSM_command("AT+QISACK");    // Check the information for the sending data
  read_response("AT+QISACK", 10, str);
  validate_response(str, "OK");

  GSM_command("AT+QICLOSE");    // Close TCP Connection
  read_response("AT+QICLOSE", 10, str);
  validate_response(str, "CLOSE OK");
}

void setup() {
  String str;
  Serial.begin(115200);

  setup_wifi();

  // setup OTA
  MDNS.begin(host);
  httpUpdater.setup(&httpServer);
  httpServer.begin();
  MDNS.addService("http", "tcp", 80);   //  Open http://[host].local/update in your browser
  httpServer.handleClient();

  // connect to mqtt server
  client.setServer(mqtt_server, 8883);
  client.setCallback(sub_callback);
  reconnect();
  String ip = String(WiFi.localIP());
  client.publish("log",WiFi.localIP().toString().c_str());

  client.publish("log","connected to wifi");
  Serial.println("connected to wifi 2");
  pinMode(GSM_PWRKEY_PIN, OUTPUT);
  digitalWrite(GSM_PWRKEY_PIN,LOW);
  delay(5000);

  GSM_setup();
  GSM_init();

  // GNSS_setup();
  // GNSS_init();
}

void loop() {
  String str, GNSS_str;

  httpServer.handleClient();
  reconnect();
  client.loop();

  GNSS_str = GNSS_read();

  str  = "POST /input2.php HTTP/1.1\r\n";
  str += "Host: www.fdli.ir\r\n";
  str += "Content-Type: application/json;\r\n";
  str += "Content-Length: " + String(GNSS_str.length()) + "\r\n";
  str += "\r\n";
  str += GNSS_str;

  TCP_send("www.fdli.ir", str);

  delay(1000);
}

// TODO:  JSON, http lib, mpu, NMEA,
